# Shusoha


## Synopsis

Shusoha est un générateur de mots de passe aléatoires doté d'une interface graphique. 


## Screenshots

| Version française | Version anglaise |
| :---: | :---: |
|  ![Shusoha Screenshot vf Here](https://framagit.org/SxncR0OR/shusoha/raw/master/shusoha-screenshot-fr.png "Shusoha")  |  ![Shusoha Screenshot vo Here](https://framagit.org/SxncR0OR/shusoha/raw/master/shusoha-screenshot-en.png "Shusoha")  |


## Exemple d'utilisation

Shusoha affiche à l'écran une série de mots de passe aléatoires tels que:

```
h9hed4tGmJ8oFK5f
jhfZ1ipRRGC6gvo4
h32AgFbenbwEBTqv
5N7lUiUNsYh6YJWB
2eifpu8JNvEYyzQM
...
```

Vous pouvez aussi choisir le jeu de caractères et la longueur du mot de passe.


## Jeux de caractères disponibles (charsets)

| Jeu de caractères | Longueur | Caractères disponibles |
| :---: | :---: | :---: |
| lowercase                      | 26 | abcdefghijklmnopqrstuvwxyz |
| uppercase                      | 26 | ABCDEFGHIJKLMNOPQRSTUVWXYZ |
| digits                         | 10 | 0123456789 |
| punctuation                    | 32 | !"#$%&'()*+,-./:;<=>?@[\\]&#94;&#95;&#96;{&#124;}~ |
| accented characters, lowercase   | 20 | áàäâéèëêíìïîóòöôúùüû |
| accented characters, uppercase | 20 | ÁÀÄÂÉÈËÊÍÌÏÎÓÒÖÔÚÙÜÛ |


## Pourquoi un nouveau générateur de mots de passe ?

Shusoha a été créé pour plusieurs raisons:

1. Pour créer un mot de passe, impossible de faire confiance à un site web ou à un programme ne diffusant pas ses sources !
2. Une **interface graphique** ! C'est quand même mieux que de devoir entrer à chaque fois une commande comme:  
`cat /dev/urandom | tr -dc '[:alnum:]' | fold -w 16 | head -n 8`
3. Une **routine fiable** : `urandom`.  
La technique utilisée par Shusoha est expliquée sur [Wikipedia: Random password generator > Python](https://en.wikipedia.org/wiki/Random_password_generator#Python).  
Shusoha utilise la routine Python `random.SystemRandom()` qui elle-même appelle `os.urandom()`.


## Installation

Shusoha est écrit en **langage Python** et utilise la librairie **GTK+ 3** pour l'affichage.

La plupart des distributions **Linux** ont déjà ces éléments installés avec le système.  
Le générateur de nombres aléatoires utilisé est *urandom*. Il ne nécessite aucune installation.

En bref, il suffit simplement de copier les fichiers dans un répertoire au choix.
* **shusoha.py**: le programme « Shusoha » lui-même, écrit en language Python
* **MUMApy.py**: une bibliothèque qui permet de gérer plusieurs langues dans l'application

Par mesure de facilité, vous pouvez rendre *shusoha.py* exécutable.  
Ou mieux encore: lui créer un lanceur avec une icône (une icône est fournie avec l'archive).


### Télécharger l'archive

Rendez-vous sur cette page [Framagit:shusoha](https://framagit.org/SxncR0OR/shusoha) pour télécharger l'archive.

Note: l'icone de téléchargement se trouve à droite sur la même ligne que Files, Commits, Branch, ...

### Récupérer les fichiers avec Git

D'abord vérifier que git est présent sur votre système:  
`sudo apt-get install git`

Puis récupérer Shusoha avec git:
```
cd choisissez/un/chemin/pour/l/installation
git clone https://framagit.org/SxncR0OR/shusoha.git
cd shusoha
chmod +x shusoha.py
```


## Contact

<shusoha.oliviervandecasteele@xoxy.net>


## Licence

Shusoha est distribué sous licence:  
[GNU Affero General Public License version 3 (AGPL v3)](https://www.gnu.org/licenses/agpl-3.0.en.html)

---

Shusoha is a password generator based on urandom  
Copyright (C) 2016 Olivier D.V. Vandecasteele  

This program is free software: you can redistribute it and/or modify  
it under the terms of the GNU Affero General Public License as  
published by the Free Software Foundation, either version 3 of the  
License, or (at your option) any later version.  

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU Affero General Public License for more details.  

You should have received a copy of the GNU Affero General Public License  
along with this program.  If not, see <http://www.gnu.org/licenses/>.