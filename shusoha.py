#!/usr/bin/python
# -*- coding:utf-8 -*-

'''
Shusoha is a password generator based on urandom  
Copyright (C) 2016 Olivier D.V. Vandecasteele  

This program is free software: you can redistribute it and/or modify  
it under the terms of the GNU Affero General Public License as  
published by the Free Software Foundation, either version 3 of the  
License, or (at your option) any later version.  

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU Affero General Public License for more details.  

You should have received a copy of the GNU Affero General Public License  
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

#
# IMPORT
#
# various importations
import sys, os, random, string, getopt, math, unicodedata
# pyGI
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
# MUMApy
from MUMApy import MUMAclass
muma=MUMAclass(language='en')
def _(label,lang=None):
	return muma.getMessage(label,lang)
# MUMApy translations
muma.removeLanguage(language='es')
muma.add(label="shusoha",translations=dict(en="Shusoha",fr="Shusoha"))
muma.add(label="shusoha-ready",translations=dict(en="Shusoha ready",fr="Shusoha prêt"))
muma.add(label="destroy-window",translations=dict(en="Shusoha closed: the window was closed",fr="Shusoha fermé: la fenêtre a été fermée"))
muma.add(label="quit-button-pressed",translations=dict(en="Shusoha closed: \"Quit\" button pressed",fr="Shusoha fermé: le bouton \"Quitter\" a été préssé"))
muma.add(label="show-button-pressed",translations=dict(en="\"Show\" button pressed",fr="Le bouton \"Afficher\" a été pressé"))
muma.add(label="password-length-error",translations=dict(en="Error: the length of the password requested is smaller than the number of the different charsets."))
muma.add(label="password-length-error",translations=dict(fr="Erreur: la longueur du mot de passe demandée est plus petite que le nombre minimal de jeux de caractères"))
muma.add(label="secure-passwords",translations=dict(en="A secure passwords'\n generator using <i>urandom</i>",fr="Générateur de mots de passe\n sécurisés utilisant <i>urandom</i>"))
muma.add(label="choose-charsets",translations=dict(en="Charset:",fr="Jeux de caractères:"))
muma.add(label="choose-length",translations=dict(en="Length:",fr="Longueur:"))
muma.add(label="show-button",translations=dict(en="Show",fr="Afficher"))
muma.add(label="quit-button",translations=dict(en="Quit",fr="Quitter"))
muma.add(label="password-info",translations=dict(en="Password length: {passwordLength} characters, lengthID={lengthID}",fr="Longueur du mot de passe: {passwordLength} caractères, lengthID={lengthID}"))
muma.add(label="charset-info",translations=dict(en="Charset selected: lowercase={lc} uppercase={uc} digits={d} punctuation={p} accents_lowercase={alc} accents_uppercase={auc}",fr="Jeu de caractères sélectionné: minuscules={lc} majuscules={uc} chiffres={d} ponctuation={p}  accents_lowercase={alc} accents_uppercase={auc}"))
muma.add(label="passwords-generated",translations=dict(en="Passwords generated:\n{passwordsList}",fr="Mots de passe créés:\n{passwordsList}"))
muma.add(label="passwords-hidden",translations=dict(en="[Passwords hidden]",fr="[Mots de passe cachés]"))
muma.add(label="message-dialog",translations=dict(en="Shusoha has generated the folowing passwords:",fr="Shusoha a crée les mots de passe suivants:"))
muma.add(label="entropy",translations=dict(en=u"Entropy by symbol: {entropy_by_symbol} bits\nEntropy by password: {entropy} bits",fr=u"Entropie par caractère: {entropy_by_symbol} bits\nEntropie par mot: {entropy} bits"))

class shusohaUI(object):
	def __init__(self,args):
		#	verbose levels:
		#	0: errors and important messages
		#	1: usefull informations
		#	2: debug level
		self.verboseLevel=1 # default: 1
		#	read command line arguments
		opts, others = getopt.getopt(args, "v:", ["verbosity=","lang="])
		for arg,value in opts:
			if arg=="-v" or arg=="--verbosity":
				if isinstance(value,str):
					if value.isdigit():
						verbosity=int(value)
						if verbosity>=0 and verbosity<=2:
							self.verboseLevel=verbosity
			if arg=="--lang":
				if isinstance(value,str):
					if value in muma.languages:
						muma.setLanguage(language=value)
		#	global setup
		self.directory=sys.path[0]
		self.passwordsLen={0:8,1:9,2:12,3:16,4:24,5:32,6:48,7:64}
		self.selected_len=4
		self.number_of_passwords=10
		#	ui setup
		self.ui_border=8
		self.setUI()
		self.entropy_by_symbol=''
		self.entropy=''
		#	init finished successfully
		self.message(_("shusoha-ready"),level=2)
	
	def message(self,text,level=0):
		if isinstance(text,str):
			if level<=self.verboseLevel:
				print("Shusoha:: "+text)
		return None
	
	def  on_shusoha_destroy(self, *args):
		self.message(_("destroy-window"),level=2)
		Gtk.main_quit(*args)
	
	def on_quitButton_clicked(self, *args):
		self.message(_("quit-button-pressed"),level=2)
		Gtk.main_quit(*args)
	
	def on_showButton_clicked(self, *args):
		self.message(_("show-button-pressed"),level=2)
		self.showPasswords()
		return None
	
	def on_comboboxLen_changed(self, *args):
		self.selected_len=self.comboboxLen.get_active()
		self.message(_("password-info").format(passwordLength=self.passwordsLen[self.selected_len],lengthID=self.selected_len),level=2)
		return None
	
	def setUI(self):
		self.window=Gtk.Window(title=_("shusoha"))
		self.window.set_border_width(self.ui_border*2)
		self.window.connect("delete-event", self.on_shusoha_destroy)
		#
		# container global vertical
		self.container_global=Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=self.ui_border)
		self.container_global.set_homogeneous(False)
		self.window.add(self.container_global)
		# Label on top
		self.labelTitle=Gtk.Label()
		self.labelTitle.set_markup(_("secure-passwords"))
		self.labelTitle.set_justify(Gtk.Justification.CENTER)
		self.container_global.pack_start(child=self.labelTitle, expand=True, fill=False, padding=0)
		# Separator
		self.separator1=Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
		self.container_global.pack_start(child=self.separator1, expand=True, fill=True, padding=0)
		#
		# container charsets + length
		self.container_charset_length=Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
		self.container_charset_length.set_homogeneous(False)
		self.container_global.pack_start(child=self.container_charset_length, expand=True, fill=True, padding=0)
		#
		# container charsets
		self.container_charsets=Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
		self.container_charsets.set_homogeneous(True)
		self.container_charset_length.pack_start(child=self.container_charsets, expand=True, fill=False, padding=0)
		# Label charsets
		self.labelCharset=Gtk.Label()
		self.labelCharset.set_markup(_("choose-charsets"))
		self.container_charsets.pack_start(child=self.labelCharset, expand=True, fill=True, padding=0)
		#
		# container for all charset toggles
		self.container_toggles=Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
		self.container_toggles.set_homogeneous(True)
		self.container_charsets.pack_start(child=self.container_toggles, expand=True, fill=True, padding=0)
		#
		# container lowercase + uppercase
		self.container_lowercase_uppercase=Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
		self.container_lowercase_uppercase.set_homogeneous(True)
		self.container_toggles.pack_start(child=self.container_lowercase_uppercase, expand=True, fill=True, padding=0)
		# ToggleButton lowercase
		self.toggleButton_lowercase=Gtk.ToggleButton(label="abc")
		self.toggleButton_lowercase.set_active(True)
		self.container_lowercase_uppercase.pack_start(child=self.toggleButton_lowercase, expand=True, fill=True, padding=0)
		# ToggleButton uppercase
		self.toggleButton_uppercase=Gtk.ToggleButton(label="ABC")
		self.toggleButton_uppercase.set_active(True)
		self.container_lowercase_uppercase.pack_start(child=self.toggleButton_uppercase, expand=True, fill=True, padding=0)
		#
		# container digits + punctuation
		self.container_digits_punctuation=Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
		self.container_digits_punctuation.set_homogeneous(True)
		self.container_toggles.pack_start(child=self.container_digits_punctuation, expand=True, fill=True, padding=0)
		# ToggleButton digits
		self.toggleButton_digits=Gtk.ToggleButton(label="123")
		self.toggleButton_digits.set_active(True)
		self.container_digits_punctuation.pack_start(child=self.toggleButton_digits, expand=True, fill=True, padding=0)
		# ToggleButton punctuation
		self.toggleButton_punctuation=Gtk.ToggleButton(label="@/*")
		self.toggleButton_punctuation.set_active(False)
		self.container_digits_punctuation.pack_start(child=self.toggleButton_punctuation, expand=True, fill=True, padding=0)
		#
		# container extras
		self.container_extras=Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
		self.container_extras.set_homogeneous(True)
		self.container_toggles.pack_start(child=self.container_extras, expand=True, fill=True, padding=0)
		# ToggleButton accented characters lowcase
		self.toggleButton_accents_lowercase=Gtk.ToggleButton(label=u'áèïôú')
		self.toggleButton_accents_lowercase.set_active(False)
		self.container_extras.pack_start(child=self.toggleButton_accents_lowercase, expand=True, fill=True, padding=0)
		# ToggleButton accented characters uppercase
		self.toggleButton_accents_uppercase=Gtk.ToggleButton(label=u'ÁÈÏÔÚ')
		self.toggleButton_accents_uppercase.set_active(False)
		self.container_extras.pack_start(child=self.toggleButton_accents_uppercase, expand=True, fill=True, padding=0)
		#
		# Separator charset + length
		self.separator_charset_and_length=Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
		self.container_charset_length.pack_start(child=self.separator_charset_and_length, expand=True, fill=True, padding=self.ui_border)
		#
		# container for length
		self.container_length=Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
		self.container_length.set_homogeneous(True)
		self.container_charset_length.pack_start(child=self.container_length, expand=True, fill=False, padding=0)
		# Label length
		self.labelLen=Gtk.Label()
		self.labelLen.set_markup(_("choose-length"))
		self.container_length.pack_start(child=self.labelLen, expand=True, fill=True, padding=0)
		# ComboBox length
		self.storeLen=list(str(x) for x in self.passwordsLen.values())
		self.comboboxLen=Gtk.ComboBoxText()
		for text in self.storeLen:
			self.comboboxLen.append_text(text)
		self.comboboxLen.set_active(self.selected_len) # self.selected_len: see __init__
		self.comboboxLen.connect("changed", self.on_comboboxLen_changed)
		self.container_length.pack_start(child=self.comboboxLen, expand=True, fill=True, padding=0)
		#
		# Separator length + buttons
		self.separator_length_and_buttons=Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
		self.container_global.pack_start(child=self.separator_length_and_buttons, expand=True, fill=True, padding=0)
		#
		# container for buttons
		self.container_buttons=Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
		self.container_buttons.set_homogeneous(True)
		self.container_global.pack_start(self.container_buttons, expand=False, fill=True, padding=0)
		# Button Show
		self.showButton=Gtk.Button.new_with_label(_("show-button"))
		self.showButton.connect("clicked", self.on_showButton_clicked)
		self.container_buttons.pack_start(child=self.showButton, expand=True, fill=True, padding=0)
		# Button Quit
		self.quitButton=Gtk.Button.new_with_label(_("quit-button"))
		self.quitButton.connect("clicked", self.on_quitButton_clicked)
		self.container_buttons.pack_start(child=self.quitButton, expand=True, fill=True, padding=0)
		#
		# Show the main window
		self.window.show_all()
		#
		#
		# Initialize the popup window
		self.popup=Gtk.MessageDialog(title=_("message-dialog"), buttons=Gtk.ButtonsType.CLOSE, message_type=Gtk.MessageType.INFO)
		self.popup.format_secondary_text('-')
		self.popup.get_message_area().get_children()[1].set_selectable(True) # allow to select and copy text in clipboard
		return None
	
	def getPassword(self, length=12, lowercase=True, uppercase=True, digits=True, punctuation=True, accents_lowercase=False, accents_uppercase=False, verify=True):
		data=None
		chaos=random.SystemRandom()
		charsetFlags=[lowercase,uppercase,digits,punctuation,accents_lowercase, accents_uppercase]
		charsetStrings=[string.ascii_lowercase,string.ascii_uppercase,string.digits,string.punctuation,u'áàäâéèëêíìïîóòöôúùüû',u'ÁÀÄÂÉÈËÊÍÌÏÎÓÒÖÔÚÙÜÛ'] # see also https://docs.python.org/2/library/string.html
		numberOfSelectedCharsets=len(list(x for x in charsetFlags if x==True))
		charsetsList=list(x[1] for x in zip(charsetFlags,charsetStrings) if x[0]==True)
		charset=str().join(charsetsList)
		entropy_by_symbol=math.log(len(charset),2)
		self.entropy_by_symbol=round(entropy_by_symbol,1)
		self.entropy=round(entropy_by_symbol*length,1)
		self.message(_("entropy").format(entropy_by_symbol=self.entropy_by_symbol,entropy=self.entropy),level=2)
		if verify and length>=numberOfSelectedCharsets:
			verifyFlag=False
			while not verifyFlag:
				data=str().join(chaos.choice(charset) for x in range(length)) # see also https://en.wikipedia.org/wiki/Random_password_generator#Python
				# verify if each charset selected is used ?
				verifyFlag=True
				for currentCharset in charsetsList:
					currentSet=False
					for c in currentCharset:
						if c in data:
							currentSet=True
							break
					if currentSet==False:
						verifyFlag=False
		else:
			data=str().join(chaos.choice(charset) for x in range(length))
		if verify and length<numberOfSelectedCharsets:
			self.message(_("password-length-error"),level=0)
		return data
	
	def showPasswords(self):
		# Get the length
		lengthID=self.comboboxLen.get_active()
		passwordLength=self.passwordsLen[lengthID]
		self.message(_("password-info").format(passwordLength=passwordLength,lengthID=lengthID), level=2)
		# Get the charset
		toggle_lowercase=self.toggleButton_lowercase.get_active()
		toggle_uppercase=self.toggleButton_uppercase.get_active()
		toggle_digits=self.toggleButton_digits.get_active()
		toggle_punctuation=self.toggleButton_punctuation.get_active()
		toggle_accents_lowercase=self.toggleButton_accents_lowercase.get_active()
		toggle_accents_uppercase=self.toggleButton_accents_uppercase.get_active()
		self.message(_("charset-info").format(lc=self.toggleButton_lowercase.get_active(), uc=self.toggleButton_uppercase.get_active(), d=self.toggleButton_digits.get_active(), p=self.toggleButton_punctuation.get_active(), alc=self.toggleButton_accents_lowercase.get_active(), auc=self.toggleButton_accents_uppercase.get_active()), level=2)
		# Create the passwords
		currentPassword=''
		passwordsList=''
		for i in range(self.number_of_passwords):
			currentPassword=self.getPassword(length=passwordLength, lowercase=self.toggleButton_lowercase.get_active(), uppercase=self.toggleButton_uppercase.get_active(), digits=self.toggleButton_digits.get_active(), punctuation=self.toggleButton_punctuation.get_active(), accents_lowercase=self.toggleButton_accents_lowercase.get_active(), accents_uppercase=self.toggleButton_accents_uppercase.get_active())
			passwordsList+=currentPassword+u'\n'
		passwordsList=passwordsList[:-1]
		currentPassword=''
		passwordsList+=u"\n\n"+_("entropy").format(entropy_by_symbol=self.entropy_by_symbol,entropy=self.entropy)
		# Show the list of passwords
		stdoutPasswords=False # Set to True for sending the passwords to the standart output. For debugging purposes only: security issues.
		if stdoutPasswords:
			self.message(_("passwords-generated").format(passwordsList=passwordsList), level=2)
		else:
			self.message(_("passwords-hidden"), level=2)
		self.popup.format_secondary_text(passwordsList)
		self.popup.show()
		self.popup.run()
		self.popup.format_secondary_text('-')
		self.popup.hide()
		passwordsList=''

if __name__ == "__main__":
	shusoha=shusohaUI(sys.argv[1:])
	Gtk.main()
