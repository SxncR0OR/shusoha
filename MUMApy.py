#!/usr/bin/python
# -*- coding:utf-8 -*-

class MUMAclass:
	"""
	MUMApy is a python library allowing to manage several languages directly in your code.
	Licence: GNU Affero General Public License (AGPL) version 3.
	Copyright (C) 2016 Olivier D.V. Vandecasteele .
	
	MUMApy follows the syntax of *gettext* but translations will be stored in the code, not in external files.
	
	How to import the module in your code? Add theses lines at the begining of your code:
	
	from MUMApy import MUMAclass
	muma=MUMAclass(language='en')
	def _(label,lang=None):
		return muma.getMessage(label,lang)
	
	How to use MUMA in my code? Change every string like 'Hello' by _('hello'). e.g. print(_('hello'))
	How to change the default language ? Modify the line: MUMAclass(language='en')
	How to add a new translation? muma.add(label="hello",translations=dict(en="Hello",fr="Bonjour",es="Hola"))
	How to add a new language ? muma.addLanguage(language='xx')
	How to change the language used after initialisation? muma.setLanguage(language='fr')
	How to force the language locally? Use the function with the <lang> option: e.g. print(_('hello'), lang='fr')
	"""
	def __init__(self,language='en'):
		self.languages=[] # the list of the used languages in the dictionary
		self.addLanguage(language='en')
		self.addLanguage(language='fr')
		self.addLanguage(language='es')
		self.translations={} # translations are stored here
		# Please don't remove the following translations. They are error messages used by the class itself.
		self.add(label="unknown-label",translations=dict(en="Error: unknown label",fr="Erreur: étiquette inconnue",es="Error: etiqueta desconocida"))
		self.add(label="unknown-language",translations=dict(en="Error: unknown language",fr="Erreur: langage inconnu",es="Error: idioma desconocido"))
		self.add(label="missing-translation",translations=dict(en="Error: missing translation for : \"{label}\": {original}",fr="Erreur: traduction manquante pour : \"{label}\": {original}",es="Error: traducción faltante para : \"{label}\": {original}"))
		self.add(label="empty-translation-list",translations=dict(en="the translation list is empty!",fr="la liste de traduction est vide !",es="¡ el lista de traducción es vacía !"))
		self.add(label="missing-item",translations=dict(en="missing item",fr="élément manquant",es="elemento faltante"))
		self.add(label="not-a-dictionnary",translations=dict(en="The <translations> argument must be a dictionnary - e.g.: translations=dict(en=\"Hello\",fr=\"Bonjour\",[...])", fr="L'argument <translations> doit être un dictionnaire - ex.: translations=dict(en=\"Hello\",fr=\"Bonjour\",[...])", es="El argumento <translations> debe ser un diccionario - Por ejemplo: translations=dict(en=\"Hello\",fr=\"Bonjour\",[...])"))
		self.add(label="language-must-be-a-string",translations=dict(en="Error: language must have the <string> type",fr="Erreur: le langage doit avoir le type <string> (chaîne de caractères)",es="Error: de idioma debe tener el tipo <string> (Cadena de caracteres)"))
		self.add(label="label-must-be-a-string",translations=dict(en="Error: the label must have the <string> type",fr="Erreur: le label doit avoir le type <string> (chaîne de caractères)",es="Error: de etiqueta debe tener el tipo <string> (Cadena de caracteres)"))
		# end of translations used by the class
		self.recovery_language='en' # recovery_language is the language used when something is wrong in the translations
		self.current_language=self.recovery_language # current_language will be modified later by .setLanguage()
		self.setLanguage(language)
	
	def add(self,label,translations):
		"""Add a new translation for a label"""
		if isinstance(label,str):
			if isinstance(translations,dict):
				if not label in self.translations:
					self.translations[label]={}
				for translation in translations.items():
					self.translations[label][translation[0]]=translation[1]
			else:
				print( self.getMessage('not-a-dictionnary') )
		else:
			print( self.getMessage('label-must-be-a-string') )
		return None
	
	def addLanguage(self,language=None):
		"""Add the selected language"""
		if not language==None:
			if isinstance(language,str):
				if not language in self.languages:
					self.languages.append(language)
			else:
				print( self.getMessage('language-must-be-a-string') )
		return None
	
	def removeLanguage(self,language=None):
		"""Remove the selected language"""
		if not language==None:
			if isinstance(language,str):
				if language in self.languages:
					for label in self.translations.keys():
						if language in self.translations[label]:
							del self.translations[label][language]
					self.languages.remove(language)
				else:
					print( self.getMessage('unknown-language') )
			else:
				print( self.getMessage('language-must-be-a-string') )
		return None
	
	def existLanguage(self,language):
		"""Test if the language is allowed"""
		allowed=language in self.languages
		if not allowed:
			print( self.getMessage('unknown-language') )
		return allowed
	
	def setLanguage(self,language):
		"""Change the used language"""
		if self.existLanguage(language):
			self.current_language=language
		return None
	
	def getMessage(self,label,language=None):
		"""Returns a translated string"""
		current_language=self.current_language
		if not language==None:
			if self.existLanguage(language):
				current_language=language
		data=None
		if label in self.translations:
			if current_language in self.translations[label]:
				data=self.translations[label][current_language]
			else:
				if len(self.translations[label])>=1:
					if self.recovery_language in self.translations[label]:
						data=self.translations['missing-translation'][current_language].format(label=label,original=self.translations[label][self.recovery_language])
					else:
						data=self.translations['missing-translation'][current_language].format(label=label,original='<'+self.translations['missing-item'][current_language]+'>')
				else:
					data=self.translations['missing-translation'][current_language].format(label=label,original=self.translations['empty-translation-list'][current_language])
		else:
			 data=self.translations['unknown-label'][current_language]
		return data
# end
